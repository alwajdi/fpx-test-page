import { Component, OnInit } from '@angular/core';
import {FpxInfo} from '../fpx.info';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-fpx',
  templateUrl: './fpx.component.html',
  styleUrls: ['./fpx.component.css']
})
export class FpxComponent implements OnInit {

  environment = 'staging';

  fpxInfo: FpxInfo = {
    amount: 155,
    buyerEmail: 'alwajdi@yopmail.com',
    description: 'test purchase',
    sellerOrderNo: '112233',
    originalUrl: window.location.href,
    fullJson: `{
      "errorCode": "SS001",
      "errorMessage": null,
      "id": "b32fea2b-7cea-4a57-a531-cba005323d3e",
      "createdBy": "5f9c1f06-e9b5-432d-bcf8-f2ccc1fc5100",
      "lastModifiedBy": "5f9c1f06-e9b5-432d-bcf8-f2ccc1fc5100",
      "createdDate": 1579058631000,
      "lastModifiedDate": 1579058631000,
      "deleted": false,
      "active": true,
      "version": 0,
      "exemptedServices": null,
      "fk_serviceId": "739404c6-3746-11ea-9d3d-0200c0a81e49",
      "flowHistory": {
        "errorCode": null,
        "errorMessage": null,
        "id": "739404c6-3746-11ea-9d3d-0200c0a81e49",
        "createdBy": "5f9c1f06-e9b5-432d-bcf8-f2ccc1fc5100",
        "lastModifiedBy": "5f9c1f06-e9b5-432d-bcf8-f2ccc1fc5100",
        "createdDate": 1579058631000,
        "lastModifiedDate": 1579058670000,
        "deleted": false,
        "active": true,
        "version": 0,
        "exemptedServices": null,
        "applicationId": "7393b372-3746-11ea-9d3d-0200c0a81e49",
        "application": {
          "errorCode": null,
          "errorMessage": null,
          "id": "7393b372-3746-11ea-9d3d-0200c0a81e49",
          "createdBy": "5f9c1f06-e9b5-432d-bcf8-f2ccc1fc5100",
          "lastModifiedBy": "5f9c1f06-e9b5-432d-bcf8-f2ccc1fc5100",
          "createdDate": 1579058631000,
          "lastModifiedDate": 1579058631000,
          "deleted": false,
          "active": true,
          "version": 0,
          "exemptedServices": null,
          "applicationType": {
            "errorCode": null,
            "errorMessage": null,
            "id": "28826f11-d2aa-11e8-8d74-7b70fa0ffe9b",
            "createdBy": "a6ef4d81-744e-49e7-9e1d-ab74a14935d6",
            "lastModifiedBy": "a6ef4d81-744e-49e7-9e1d-ab74a14935d6",
            "createdDate": 1539848890000,
            "lastModifiedDate": 1539848890000,
            "deleted": null,
            "active": true,
            "version": 0,
            "exemptedServices": null,
            "entityName": null,
            "code": "48",
            "descriptionMys": "PERMOHONAN PENGIKTIRAFAN MAKMAL SWASTA",
            "descriptionEng": "PERMOHONAN PENGIKTIRAFAN MAKMAL SWASTA",
            "status": null,
            "applicationTypeId": null,
            "formId": null,
            "formName": null,
            "certificate": null
          },
          "appType": "PERMOHONAN PENGIKTIRAFAN MAKMAL SWASTA",
          "subApplicationTypeDescEng": null,
          "subApplicationTypeDescMys": null,
          "subApplicationTypeCode": null,
          "triggerType": {
            "errorCode": null,
            "errorMessage": null,
            "id": "f2a5855a-93a5-11e7-92ef-e12219afccb3",
            "createdBy": "a6ef4d81-744e-49e7-9e1d-ab74a14935d6",
            "lastModifiedBy": "a6ef4d81-744e-49e7-9e1d-ab74a14935d6",
            "createdDate": 1539675648000,
            "lastModifiedDate": 1539675648000,
            "deleted": null,
            "active": true,
            "version": 0,
            "exemptedServices": null,
            "entityName": null,
            "code": "101",
            "descriptionMys": "PERMOHONAN / PENDAFTARAN BARU",
            "descriptionEng": "APPLICATION / NEW REGISTRATION",
            "status": null,
            "triggerTypeId": null
          },
          "fk_appId": "65c102b6-7299-4e28-bcce-d3d94cac3650",
          "processFlow": null,
          "applicationNo": "MS012000002-0",
          "approvalReferenceNo": null,
          "approvalDate": null,
          "validityDate": null,
          "certificateVersion": 0,
          "payment1Flag": null,
          "payment2Flag": null,
          "status": "INPROGRESS",
          "company": null,
          "premiseSubCategory": null,
          "state": null,
          "district": null,
          "renewFlag": null,
          "fk_parentId": null,
          "completeFlag": false
        },
        "applicationApprovalProductId": null,
        "approvalProduct": null,
        "triggerTypeId": "f2a5855a-93a5-11e7-92ef-e12219afccb3",
        "triggerType": {
          "errorCode": null,
          "errorMessage": null,
          "id": "f2a5855a-93a5-11e7-92ef-e12219afccb3",
          "createdBy": "a6ef4d81-744e-49e7-9e1d-ab74a14935d6",
          "lastModifiedBy": "a6ef4d81-744e-49e7-9e1d-ab74a14935d6",
          "createdDate": 1539675648000,
          "lastModifiedDate": 1539675648000,
          "deleted": null,
          "active": true,
          "version": 0,
          "exemptedServices": null,
          "entityName": null,
          "code": "101",
          "descriptionMys": "PERMOHONAN / PENDAFTARAN BARU",
          "descriptionEng": "APPLICATION / NEW REGISTRATION",
          "status": null,
          "triggerTypeId": null
        },
        "referenceNo": "MS012000002-0",
        "isLaporanAkhir": false,
        "completeFlag": false,
        "hasSampling": null,
        "hasAudit": null,
        "hasInspection": null,
        "payment1Flag": null,
        "payment2Flag": null,
        "stateId": null,
        "districtId": null,
        "status": "SUBMIT",
        "parentId": null
      },
      "application": null,
      "discount": null,
      "total": "500.0",
      "totalAfterDiscount": null,
      "applicantName": "PEMOHON BESS 1",
      "companyName": null,
      "premiseName": null,
      "applicantId": "5f9c1f06-e9b5-432d-bcf8-f2ccc1fc5100",
      "companyId": null,
      "premiseSubCategoryId": null,
      "paymentOccurence": "1",
      "paymentOrderDetailsList": [
        {
          "errorCode": null,
          "errorMessage": null,
          "id": "0ecf1891-2ef9-4316-9fce-97a2d7af06ed",
          "createdBy": "5f9c1f06-e9b5-432d-bcf8-f2ccc1fc5100",
          "lastModifiedBy": "5f9c1f06-e9b5-432d-bcf8-f2ccc1fc5100",
          "createdDate": 1579058631000,
          "lastModifiedDate": 1579058631000,
          "deleted": false,
          "active": true,
          "version": 0,
          "exemptedServices": null,
          "paymentItem": {
            "errorCode": null,
            "errorMessage": null,
            "id": "e19594fd-d0ff-11e8-9b9a-00155d4d4cdf",
            "createdBy": null,
            "lastModifiedBy": null,
            "createdDate": 1579058772080,
            "lastModifiedDate": 1579058772080,
            "deleted": null,
            "active": null,
            "version": null,
            "exemptedServices": null,
            "description": "Pengiktirafan Makmal Swasta (Permohonan Baru)",
            "ruleCode": null,
            "fosimPaymentCode": null,
            "price": 500
          },
          "quantity": "1",
          "unitPrice": "500.00",
          "total": "500.0"
        }
      ]
    }
    `
  };

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit() {
  }

  pay(): void {
    if(this.environment === 'staging'){
      (document.querySelector('#fpxForm') as HTMLFormElement).submit();
    } else {
      console.log('async dummy code will be invoked!');
    }
  }

}
