import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FpxComponent } from './fpx.component';

describe('FpxComponent', () => {
  let component: FpxComponent;
  let fixture: ComponentFixture<FpxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FpxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FpxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
