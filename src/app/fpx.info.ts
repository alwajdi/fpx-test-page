export class FpxInfo {
    amount: number;
    sellerOrderNo: string;
    originalUrl: string;
    description: string;
    buyerEmail: string;
    fullJson: string;
}